# Compro Code Examples

The code examples that are presented in the Competitive Programming lecture.

The following snippets are currently available:

* `template.cpp`: A template for C++ submissions.
* `template.rs`: A template for Rust submissions, containing some useful basic I/O.
* `compilation/`: The scripts used for compilation on our online judge.
* `lectures/`: Examples from the lecture slides. **WARNING**: The code is untested. It may contain bugs. It was also shortened to fit on the slides and therefore uses too few curly braces and too many global variables.
* `tutorials/`: Some files used in the tutorials.
