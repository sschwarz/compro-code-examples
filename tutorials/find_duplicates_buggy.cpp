#include <bits/stdc++.h>
using namespace std;

bool found_duplicate = false;
bool character_found[26];

int main()
{
    string input;
    cin >> input;

    for (char c: input) {
        // transform c to a number between 0 (a) and 25 (z)
        if (c >= 'a' && c <= 'z') c -= 96;
        else if (c >= 'A' && c <= 'Z') c -= 64;
        else assert(false); // invalid input

        if (character_found[c]) found_duplicate = true;
        else character_found[c] = true;
    }

    if (found_duplicate) cout << "duplicate\n";
    else cout << "no duplicate\n";
}
