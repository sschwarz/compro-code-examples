#!/bin/bash

green="\u001b[32m"
red="\u001b[31m"
reset="\u001b[0m"

# The g++ compiler calls your compiled binary `a.out`.
# So it's sensible to not call the answer files `.out`, but rather `.ans`.
#
# For this script, you should create file pairs called testname.{in,ans} 
#
# code.cpp
# a.out
# 1.in   # first test
# 1.ans
# 2.in   # second test
# 2.ans

# We iterate through all .in files
for inputfile in `ls -v *.in`; do
    # $solfile is the name of the file containing the answer to $inputfile
    solfile=$( echo $inputfile | sed 's/\.in/\.ans/')
    echo -ne "$inputfile\t"
    # We feed the $inputfile into your program and write the result to /tmp/runtests_output
    ( cat $inputfile |  ./a.out 2> /dev/null) > /tmp/runtests_output
    # We compute the textual difference between /tmp/runtests_output and $solfile
    cat /tmp/runtests_output | diff -Z "$solfile" - >> /dev/null
    if [[ $? == 0 ]]; then
        # they agree, test passed
        echo -e "${green}passed${reset}"
    else
        # They disagre, test failed
        echo -e "${red}output differs${reset}:"
        # Print the actual and expected outputs side by side
        paste $solfile /tmp/runtests_output | sed 's/^/\t/g'
    fi
done
