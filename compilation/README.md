In this folder are the exact scripts that are used to compile your submission on our online judge.

All scripts take the following three parameters:

* The destination where the executable should be stored
* The memory limit for this task (usually ignored for compilation)
* The files (at least one) to compile

We do **not** recommend using these scripts for building on your machine. However, when compilation errors occur, it may be useful to re-check these scripts.

