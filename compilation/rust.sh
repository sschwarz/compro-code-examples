#!/bin/sh

# Rustc version: rustc 1.77.1 (7cf61ebde 2024-03-27)

# Rust compile wrapper-script for 'compile.sh'.

DEST="$1" ; shift
MEMLIMIT="$1" ; shift

# This (obviously) is a hack for the server side. Locally, you usually install in ~/.cargo or ~/.rustup
export CARGO_HOME=/usr/bin/.cargo
export RUSTUP_HOME=/usr/bin/.rustup

mkdir rust_tmp_dir
export TMPDIR=rust_tmp_dir
rustc --edition=2021 -C opt-level=3 -o "$DEST" "$@"
rmdir rust_tmp_dir
exit 0
