#!/bin/sh

# g++ version
# g++ (Debian 8.3.0-6) 8.3.0

# C++ compile wrapper-script for 'compile.sh'.

DEST="$1" ; shift
MEMLIMIT="$1" ; shift

# -x c++:       Explicitly set compile language to C++ (no object files or
#               other languages autodetected by extension)
# -std=gnu++17: Set C++ version to C++17, with GNU extensions
# -Wall:        Report all warnings
# -O2:          Level 2 optimizations (default for speed)
# -static:      Static link with all libraries
# -pipe:        Use pipes for communication between stages of compilation
g++ -x c++ -std=gnu++17 -Wall -O2 -static -pipe -o "$DEST" "$@"
exit $?
