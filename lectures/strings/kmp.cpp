#include <bits/stdc++.h>
#define int long long

using namespace std;

string s, t;
int n, m;

int32_t main() {
    cin >> s >> t;
    n = s.size(); m = t.size();

    vector<int> lsp(m, 0);
    for (int i = 1, prev = 0; i < m; ) {
      if (t[i] == t[prev]) {
        prev++;
        lsp[i] = prev;
        i++; 
      } else if (prev == 0) {
        lsp[i] = 0;
        i++;
      } else { prev = lsp[prev-1]; }
    }

    int start = 0, len = 0;
    while (start + len < n) {
        while (len >= m || s[start+len] != t[len]) {
            if (len == 0) { start++; len = -1; break; }
            int skip = len - lsp[len-1];
            start += skip; len -= skip;
        }
        len++;
        if (len == m)
            cout << "t matches s at " << start << "\n";
    }
}
