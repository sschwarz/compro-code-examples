#include <bits/stdc++.h>
#define int long long

using namespace std;

// Implementation of a trie node for Aho-Corasick

// The "cout << ..." command can be replaced by something different
// (e.g. by something that puts the number in a result vector).

// Note that the number of matches will also be printed for position -1,
// which equals the number of empty strings fed into the algorithm
// (empty strings already match at the beginning of the string, even when
// no character has been read yet).
struct ACTrie {

    map<char, ACTrie*> edges; // Outgoing edges of the trie node
    ACTrie* lsp = nullptr; // Longest Suffix-Prefix

    // Number of input strings being a suffix
    // of the string associated with this trie node.
    int cnt = 0;

    void insert(string &t, int i = 0) {
        if (i == t.length()) {
            cnt++;
            return;
        }
        if (edges.count(t[i]) == 0)
            edges[t[i]] = new ACTrie;
        edges[t[i]]->insert(t, i+1);
    }

    // Searches at the current node for matches in the string s[i..].
    // 'print' denotes whether the number of matches should be printed
    // for the current position. The only case when we don't want this is
    // when an LSP-jump has been done, and the correct result for i has already
    // been printed.
    void search(string &s, int i = 0, bool print = true) {
        if (print) cout << cnt << " matches ending at " << i-1 << "\n";

        if (i == s.length()) return; // processing of the string is done

        if (edges.count(s[i]) == 0) {
            // The trie node doesn't have the needed character edge...
            if (lsp == nullptr) search(s, i+1, true); // we are at the root node
            else lsp->search(s, i, false); // try to continue search at the LSP
        } else {
            // Edge was found, continue search there and advance the string
            // pointer by one...
            edges[s[i]]->search(s, i+1, true);
        }
    }
};

// Should be called after inserting strings into the trie and before searching
// for matches in another string.
void preprocess(ACTrie &root) {
    queue<ACTrie*> q;
    root.lsp = nullptr; q.push(&root);
    while (!q.empty()) {
        ACTrie *u = q.front(); q.pop();
        for (auto it : u->edges) { // edge u--c-->v
            char c = it.first; ACTrie *v = it.second;

            // the 'lsp' and 'cnt' values of v will be calculated now...
            ACTrie *l = u->lsp;
            while (l != nullptr && l->edges.count(c) == 0)
                l = l->lsp;
            if (l == nullptr) {
                v->lsp = &root; // there is no strict suffix-prefix
            } else {
                v->lsp = l->edges[c];
                v->cnt += v->lsp->cnt;
            }
            q.push(v);
        }
    }
}


// The following code shows how to use the implementation above.
// As an example, it first reads the small strings that will be matched.
// It follows a list of large strings, in which all matches of the small
// strings are to be found.

// Example Input:
/*
6
baba
b
abab
ababac
abaca
ac
1
abaababcababaca
*/
int32_t main() {
    ACTrie root;

    int k; cin >> k;
    for (int i = 0; i < k; ++i) {
        string t; cin >> t;
        root.insert(t);
    }
    preprocess(root);

    int l; cin >> l;
    for (int i = 0; i < l; ++i) {
        string s; cin >> s;
        root.search(s);
    }
}
