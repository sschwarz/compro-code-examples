#include <bits/stdc++.h>
#define int long long

using namespace std;

struct trie {
    bool isEndOfString = false;
    map<char, trie*> edges;
    void insert(string &s, int i = 0) {
        if (i == s.length()) {
            isEndOfString = true;
            return;
        }
        if (edges.count(s[i]) == 0)
            edges[s[i]] = new trie;
        edges[s[i]]->insert(s, i+1);
    }
    bool contains(string &s, int i = 0) {
        if (i == s.length())
            return isEndOfString;
        return edges.count(s[i]) > 0 &&
            edges[s[i]]->contains(s, i+1);
    }
};

trie t;

int32_t main() {
    int n; cin >> n;
    for (int i = 0; i < n; ++i) {
        string s; cin >> s;
        t.insert(s);
    }
    int m; cin >> m;
    for (int i = 0; i < m; ++i) {
        string s; cin >> s;
        cout << t.contains(s) << "\n";
    }
}
