// Computes m^n modulo p
ll fexp(ll m, ll n, ll p) {
    if (n == 0) return 1;
    else if (n % 2 == 1)
        return (m * fexp(m, n-1, p)) % p;
    else { // n is even
        ll r = fexp(m, n/2, p);
        return (r * r) % p;
    }
}