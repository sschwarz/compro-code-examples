// a set with the features of an array
// implemented in STL, but only supported by GNU C++

#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>

using namespace __gnu_pbds;
using namespace std;

typedef tree<int, null_type, less<int>, rb_tree_tag, tree_order_statistics_node_update> ost;
// other types and comparators are also possible (eg. pair<int,int> and greater<pair<int,int>>)

int main(){
	// usage
	ost t;
	t.insert(-1); t.insert(10); t.insert(21); t.insert(42);
	cout << *t.find_by_order(3) << "\n"; // key at index 3 (42) in O(log n)
	t.insert(37);
	cout << *t.find_by_order(3) << "\n"; // now this outputs 37
	cout << t.order_of_key(42) << "\n"; // position of key 42 (4) in O(log n)
	// actually calculates number of smaller keys
}
