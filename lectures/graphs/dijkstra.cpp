#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
int INF; // a sufficiently large number
vector<vector<pair<int, int>>> adj; // the graph

void dijkstra(int start) {
    vector<int> dist(V, INF);
    dist[start] = 0;
    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int,int>>> pq;
    pq.push({0, start}); // <distance, vertex>
    while (!pq.empty()) {
        auto front = pq.top(); pq.pop();
        int d = front.first, v = front.second;
        if (d > dist[v]) continue; // lazy deletion
        for (auto p: adj[v]) { // <target, weight>
            int u = p.first;
            int w = p.second;
            if (dist[v] + w < dist[u]) {
                dist[u] = dist[v] + w;
                pq.push({dist[u], u}); // can push duplicate vertices
            }
        }
    }
}
