#include <bits/stdc++.h>

/*
 * sample input:
4 4
1 4
2 3
3 4
2 4
 * sample output:
0 : 1
1 : 1
2 : 1
3 : 0
 */

using namespace std;

int main(){
	int n, m;
	cin >> n >> m;
	set<pair<int,int>> edges;
	for(int i = 0; i < m; i++){
		int u, v;
		cin >> u >> v;
		u--, v--;
		edges.insert({u,v}), edges.insert({v,u});
	}

	vector<vector<bool>> dp(1<<n, vector<bool>(n, false)); // subset, endnode

	for(int i = 0; i < n; i++){
		dp[1 << i][i] = true;
	}

	for(int X = 1; X < (1<<n); X++){ // all sets
	if(__builtin_popcount(X) < 2) continue;

	for(int v = 0; v < n; v++) {   // all ending vertices
		if(!(X & (1<<v))) continue;  // v not in X

		for(int u = 0; u < n; u++) {
		if(dp[X ^ (1<<v)][u] && edges.count({u, v})){
			dp[X][v] = true;
		}
		}
	}
	}
	for(int i = 0; i < n; i++){
		cout << i << " : " << dp[(1<<n)-1][i] << "\n";
	}
}
