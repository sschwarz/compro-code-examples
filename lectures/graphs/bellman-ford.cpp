#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
int INF; // a sufficiently large number
vector<vector<pair<int, int>>> adj; // the graph

void bellman_ford(int start)
{
    vector<int> dist(V, INF);
    dist[start] = 0;
    for (int i = 0; i < V - 1; i++) {
        for (int v = 0; v < V; v++) {
            for (auto p: adj[v]) {
                int u = p.first;
                int w = p.second;
                dist[u] = min(dist[u], dist[v] + w);
            }
        }
    }
}
