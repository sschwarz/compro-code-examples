#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
vector<vector<int>> adj; // the graph

void dfs(int start) {
    vector<bool> visited(V, false);
    stack<int> S;          // LIFO

    S.push(start);         //
    visited[start] = true; // start vertex
    while (!S.empty()) {
        int v = S.top(); S.pop();
        for (int u: adj[v])
            if (!visited[u]) {
                S.push(u);
                visited[u] = true;
            }
    }
}
