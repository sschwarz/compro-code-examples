#include<bits/stdc++.h>

using namespace std;

typedef tuple<int, int, int> queue_entry;

void visit(int v, vector<bool>& visited, vector<vector<pair<int,int>>>& adj, priority_queue<queue_entry, vector<queue_entry>, greater<queue_entry>>& PQ) {
    visited[v] = true;
    for (auto p: adj[v]) {
        int u = p.first;
        int w = p.second;
        if (!visited[u]) PQ.push({w, v, u});
    }
}

int main() {
    int V, E;
    cin >> V >> E;
    vector<vector<pair<int,int>>> adj(V);

    // read adj
    int from, to, weight;
    for (int i = 0; i < E; i++) {
        cin >> from >> to >> weight;
        adj[from].push_back({to, weight});
        adj[to].push_back({from, weight});
    }

    vector<bool> visited(V, false);
    // <weight, from, to>
    priority_queue<queue_entry, vector<queue_entry>, greater<queue_entry>> PQ;
    visit(0, visited, adj, PQ);
    while (!PQ.empty()) {
        auto front = PQ.top(); PQ.pop();
        int w, from, to;
        tie(w, from, to) = front;
        if (!visited[to]){
            cout << "Add " << from << "-" << to << " to MST\n";
            visit(to, visited, adj, PQ);
        }
    }
}


