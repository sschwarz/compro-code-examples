#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
vector<vector<int>> adj; // the graph
vector<bool> visited(V, false);
deque<int> ts; // the final topological sort

void dfs(int v) { // modified dfs for toposort
    visited[v] = true;
    for (int u: adj[v])
        if (!visited[u])
            dfs(u);
    ts.push_front(v);
}
