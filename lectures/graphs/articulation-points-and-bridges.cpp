#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
vector<vector<int>> adj; // the graph

int dfs_counter = 0;
const int UNVISITED = -1;
int dfsRoot, rootChildren;

vector<int> dfs_num(V, UNVISITED);
vector<int> dfs_min(V, UNVISITED);
vector<int> dfs_parent(V, -1);

void dfs(int u) {
    dfs_min[u] = dfs_num[u] = dfs_counter++;
    for (auto v: adj[u]) {
        if (dfs_num[v] == UNVISITED) { // Tree Edge
            dfs_parent[v] = u;
            if (u == dfsRoot) rootChildren++;

            dfs(v);

            if (dfs_num[u] <= dfs_min[v] && u != dfsRoot)
                cout << u << " is AP" << endl;
            if (dfs_num[u] < dfs_min[v])
                cout << u << "-" << v << " is Bridge" << endl;
            dfs_min[u] = min(dfs_min[u], dfs_min[v]);
        } else if (v != dfs_parent[u]) // Back Edge
            dfs_min[u] = min(dfs_min[u], dfs_num[v]);
    }
}

void articulation_points_and_bridges() {
for (int i = 0; i < V; i++)
    if (dfs_num[i] == UNVISITED) {
        dfsRoot = i; rootChildren = 0;
        dfs(i); // code on next slide
        if (rootChildren > 1)
            cout << i << " is AP" << endl;
    }
}
