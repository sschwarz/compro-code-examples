#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
int E; // number of edges
vector<vector<int>> adj; // the graph
vector<int> indegree; // store indegree of each vertex
deque<int> cycle;

void find_cycle(int u) {
    while (adj[u].size()) {
        int v = adj[u].back();
        adj[u].pop_back();
        find_cycle(v);
    }
    cycle.push_front(u);
}

void euler_cycle() {
    // test if solution can exist
    for (int i = 0; i < V; i++)
        if (indegree[i] != adj[i].size()) {
            cout << "IMPOSSIBLE" << endl;
            exit(0);
        }

    // start anywhere
    find_cycle(0); // populate cycle
    // test against disconnected graphs
    if (cycle.size() != E + 1) {
        cout << "IMPOSSIBLE" << endl;
        exit(0);
    }
    for (auto v: cycle)
        cout << v << " ";
    cout << endl;
}
