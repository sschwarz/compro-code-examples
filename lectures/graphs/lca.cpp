#include <bits/stdc++.h>

using namespace std;

void dfs(int u, vector<vector<int>> &g, vector<int> &h, vector<int> &p){
	for(int v : g[u]) if(v != p[u]){
		p[v] = u;
		h[v] = h[u]+1;
		dfs(v, g, h, p);
	}
}

int kth(int u, int k, vector<vector<int>> &p){
	for(int i = p.size()-1; i >= 0; i--){
		if(k & (1<<i)){
			u = p[i][u];
		}
	}
	return u;
}

int lca(int u, int v, vector<vector<int>> &p, vector<int> &h){
	if(h[v] > h[u]) swap(u, v);
	u = kth(u, h[u]-h[v], p); // lift lower node to the same height
	if(u == v) return u;

	for(int i = p.size()-1; i >= 0; i--){
		if(p[i][u] != p[i][v]){
			u = p[i][u];
			v = p[i][v];
		}
	}
	return p[0][u];
}

int main(){
	int n, root;
	vector<vector<int>> g; // graph
	int l = ceil(log2(n))+1;

	vector<int> h(n); // heights of the nodes
	vector<vector<int>> p(l, vector<int>(n));

	p[0][root] = root;
	dfs(root, g, h, p[0]); // precompute first ancestors and heights

	for(int i = 1; i < l; i++) // order of loops matters!
		for(int j = 0; j < n; j++)
			p[i][j] = p[i-1][p[i-1][j]];
}
