#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
vector<vector<int>> adj; // the graph
vector<bool> visited(V, false);

void dfs(int v) {
    visited[v] = true;
    for (int u: adj[v])
        if (!visited[u])
            dfs(u);
}
