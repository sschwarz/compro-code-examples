#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
int INF; // a sufficiently large number
vector<vector<pair<int, int>>> adj; // the graph

void spfa(int start) {
    vector<int> dist (V, INF);
    queue<int> Q;
    vector<bool> inQ (V, false);
    dist[start] = 0; Q.push(start); inQ[start] = true;
    while (!Q.empty()) {
        int v = Q.front(); Q.pop(); inQ[v] = false;
        for (auto p: adj[v]) {
            int u = p.first;
            int w = p.second;
            if (dist[u] > dist[v] + w) {
                dist[u] = dist[v] + w;
                if (!inQ[u]) {
                    Q.push(u);
                    inQ[u] = true;
                }
            }
        }
    }
}
