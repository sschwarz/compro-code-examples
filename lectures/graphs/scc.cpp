#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
vector<vector<int>> adj; // the graph

stack<int> S; // stack
int dfs_counter = 0;
const int UNVISITED = -1;

vector<int> dfs_num(V, UNVISITED);
vector<int> dfs_min(V, UNVISITED);
vector<bool> on_stack(V, false);

void dfs(int u) {
    dfs_min[u] = dfs_num[u] = dfs_counter++;
    S.push(u);
    on_stack[u] = true;
    for (auto v: adj[u]) {
        if (dfs_num[v] == UNVISITED) {
            dfs(v);
            dfs_min[u] = min(dfs_min[u], dfs_min[v]);
        }
        else if (on_stack[v]) // only on_stack can use back edge
            dfs_min[u] = min(dfs_min[u], dfs_num[v]);
    }
    if (dfs_min[u] == dfs_num[u]) { // output result
        cout << "SCC: ";
        int v = -1;
        while (v != u) { // output SCC starting in u
            v = S.top(); S.pop(); on_stack[v] = false;
            cout << v << " ";
        }
        cout << endl;
    }
}

void scc() {
    for (int i = 0; i < V; i++) {
        if (dfs_num[i] == UNVISITED)
            dfs(i); // on next slide
    }
}
