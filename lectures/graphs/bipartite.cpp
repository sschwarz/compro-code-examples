#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
vector<vector<int>> adj; // the graph
vector<int> colors(V, -1); // -1 means unvisited

void dfs(int v) {
    for (auto u: adj[v])
        if (colors[u] == -1) {
            colors[u] = 1 - colors[v];
            dfs(u);
        } else if (colors[u] == colors[v]) {
            cout << "Impossible" << endl;
            exit(0);
        }
}

void is_bipartite(int start) {
    colors[start] = 0; // colors are 0, 1
    // assume adj to be connected
    dfs(start);
    cout << "Possible" << endl;
}
