
#include<bits/stdc++.h>

using namespace std;


class UnionFind {
    private:
        vector<int> parent, rank;
    public: 
        UnionFind(int N) {
            rank.assign(N, 0);
            parent.assign(N, 0);
            for (int i = 0; i < N; i++) parent[i] = i;
        }
        int findSet(int i) {
            if (parent[i] == i)
                return i;
            else     // path compression
                return parent[i] = findSet(parent[i]);
        }
        bool isSameSet(int i, int j) {
            return findSet(i) == findSet(j);
        }
        void unionSet(int i, int j) {
            if (!isSameSet(i, j)) {
                int x = findSet(i), y = findSet(j);
                if (rank[x] > rank[y])
                    parent[y] = x;
                else {
                    parent[x] = y;
                    if (rank[x] == rank[y]) rank[y]++;
                }
            }
        }
};


int main() {
    auto u = UnionFind(5);
    u.unionSet(1,4);
    cout << u.findSet(1) << endl;
    cout << u.findSet(4) << endl;
    u.unionSet(1,3);
    cout << u.findSet(4) << endl;
    cout << u.findSet(3) << endl;
    cout << u.findSet(2) << endl;
    cout << u.findSet(1) << endl;
    cout << u.findSet(0) << endl;
}

