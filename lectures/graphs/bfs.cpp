#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
int INF; // a sufficiently large number
vector<vector<int>> adj; // the graph

void bfs(int start, vector<int> distance) {
    distance.assign(V, INF);
    queue<int> Q;
    distance[start] = 0; Q.push(start);
    while (!Q.empty()) {
        int v = Q.front(); Q.pop();
        for (int u: adj[v]) {
            if (distance[u] == INF) { // not visited
                distance[u] = distance[v] + 1;
                Q.push(u);
            }
        }
    }
}
