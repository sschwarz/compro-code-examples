
#include<bits/stdc++.h>

using namespace std;


class UnionFind {
    private:
        vector<int> parent, rank;
    public: 
        UnionFind(int N) {
            rank.assign(N, 0);
            parent.assign(N, 0);
            for (int i = 0; i < N; i++) parent[i] = i;
        }
        int findSet(int i) {
            if (parent[i] == i)
                return i;
            else     // path compression
                return parent[i] = findSet(parent[i]);
        }
        bool isSameSet(int i, int j) {
            return findSet(i) == findSet(j);
        }
        void unionSet(int i, int j) {
            if (!isSameSet(i, j)) {
                int x = findSet(i), y = findSet(j);
                if (rank[x] > rank[y])
                    parent[y] = x;
                else {
                    parent[x] = y;
                    if (rank[x] == rank[y]) rank[y]++;
                }
            }
        }
};


int main() {
    auto UF = UnionFind(5);
    vector<tuple<int, int, int>> edgeList;
    sort(edgeList.begin(), edgeList.end());
    int w, u, v;
    for (auto edge: edgeList) {
        tie(w, u, v) = edge;
        if (!UF.isSameSet(u, v)) {
            UF.unionSet(u, v);
        }
    }
}

