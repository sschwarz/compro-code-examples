#include <bits/stdc++.h>
using namespace std;

int V; // number of nodes
vector<vector<int>> adj; // the graph

int UNVISITED = 0, EXPLORED = 1, VISITED = 2;
vector<int> visited(V, UNVISITED);

void dfs(int v) {
    visited[v] = EXPLORED;
    for (auto u: adj[v])
        if (visited[u] == UNVISITED) {
            dfs(u);
        } else { // not part of dfs tree
            if (visited[u] == EXPLORED) {
                cout << "Cycle found" << endl;
                exit(0);
            }
        }
    visited[v] = VISITED;
}

void find_cycles(int start) {
    dfs(start);
    cout << "Graph is acyclic" << endl;
}
