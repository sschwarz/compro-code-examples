#include <bits/stdc++.h>
using namespace std;

bool check(long long i);

// returns minimal t so that check(t) is true
long long binary_search() {
    long long mini = -1, maxi = numeric_limits<long long>::max() / 2;
    while (mini < maxi - 1) {
        long long middle = (mini + maxi) / 2;
        if (check(middle)) maxi = middle;
        else mini = middle;
    }
    return maxi;
}
