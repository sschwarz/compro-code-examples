#include <bits/stdc++.h>
using namespace std;

// returns position in S or -1
int binary_search(vector<int>& S, int k) {
    int mini = 0, maxi = S.size();
    while (mini < maxi - 1) {
        int middle = (mini + maxi) / 2;
        if (S[middle] <= k) mini = middle;
        else maxi = middle;
    }
    if (S[mini] == k) return mini;
    else return -1;
}
