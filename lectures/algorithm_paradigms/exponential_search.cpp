#include <bits/stdc++.h>
using namespace std;

bool check(long long i);

// returns minimal t so that check(t) is true
long long exponential_search() {
    long long mini = -1, distance = 1;
    while (!check(mini + distance)) distance *= 2;
    long long maxi = mini + distance;
    while (mini < maxi - 1) {
        long long middle = (mini + maxi) / 2;
        if (check(middle)) maxi = middle;
        else mini = middle;
    }
    return maxi;
}
