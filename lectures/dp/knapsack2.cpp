#include <bits/stdc++.h>
using namespace std;

int knapsack_(vector<vector<int>> &dp, int i, int v, vector<int> &w, vector<int> &p) {
    if (dp[i][v] != -1) return dp[i][v];
    int &res = dp[i][v];
    if (i == 0) res = (v >= w[0]) ? p[0] : 0;
    else {
        res = knapsack_(dp, i-1, v, w, p);
        if (v >= w[i]) res = max(res, p[i] + knapsack_(dp, i-1, v-w[i], w, p));
    }
    return res;
}

int knapsack(int n, int maxW, vector<int> &w, vector<int> &p) {
    vector<vector<int>> dp(n, vector<int>(maxW+1, -1));
    return knapsack_(dp, n-1, maxW, w, p);
}
