#include <bits/stdc++.h>
using namespace std;

int lis(const vector<int> &a) {
    int n = a.size();
    vector<int> dp(n, 0);
    int result = 0;
    for (int i = 0; i < n; ++i) {
        dp[i] = 1;
        for (int j = 0; j < i; ++j)
            if (a[j] < a[i])
                dp[i] = max(dp[i], dp[j] + 1);
        result = max(result, dp[i]);
    }
    return result;
}
