#include <bits/stdc++.h>
using namespace std;

long long fib(int n) {
    vector<long long> dp(max(n+1, 2), 0);
    dp[0] = dp[1] = 1;
    for (int i = 2; i <= n; ++i)
        dp[i] = dp[i-1] + dp[i-2];
    return dp[n];
}
