#include <bits/stdc++.h>
using namespace std;

int knapsack(int n, int maxW, vector<int> &w, vector<int> &p) {
    vector<vector<int>> dp(n, vector<int>(maxW+1));
    for (int v = 0; v <= maxW; ++v)
        dp[0][v] = (v >= w[0] ? p[0] : 0);
    for (int i = 1; i < n; ++i) {
        for (int v = 0; v <= maxW; ++v) {
            dp[i][v] = dp[i-1][v];
            if (v >= w[i])
                dp[i][v] = max(dp[i][v], p[i] + dp[i-1][v - w[i]]);
        }
    }
    return dp[n-1][maxW];
}
