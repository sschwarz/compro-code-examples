#include <bits/stdc++.h>
using namespace std;

int lis(const vector<int> &a, deque<int> &seq) {
    int n = a.size();
    vector<int> dp(n, 0), predecessor(n, -1);
    int result = 0, lastElement = -1;
    for (int i = 0; i < n; ++i) {
        dp[i] = 1;
        for (int j = 0; j < i; ++j)
            if (a[j] < a[i] && dp[j] + 1 > dp[i]) {
                dp[i] = dp[j] + 1;
                predecessor[i] = j;
            }
        if (dp[i] > result) {
            result = dp[i];
            lastElement = i;
        }
    }
    while (lastElement != -1) {
        seq.push_front(lastElement);
        lastElement = predecessor[lastElement];
    }
    return result;
}
