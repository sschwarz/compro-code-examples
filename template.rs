use std::io::{stdin, BufRead};
use std::str::FromStr;

#[allow(unused)]
fn read_stdin_line() -> String {
    stdin()
        .lock()
        .lines()
        .next()
        .expect("unexpected eof")
        .expect("failed to read line")
}

#[allow(unused)]
fn read_stdin_parseable<T>() -> T
where
    T: FromStr,
    T::Err: std::fmt::Debug,
{
    read_stdin_line()
        .parse()
        .expect("failed to parse line into value")
}

#[allow(unused)]
fn read_line_multiple<T>() -> Vec<T>
where
    T: FromStr,
    T::Err: std::fmt::Debug,
{
    let line = read_stdin_line();
    line.split_whitespace()
        .map(|v| v.parse().expect("failed to parse value"))
        .collect()
}

fn main() {
    
}
